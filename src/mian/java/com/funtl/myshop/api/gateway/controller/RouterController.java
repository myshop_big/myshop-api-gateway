package com.funtl.myshop.api.gateway.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.rpc.RpcContext;
import com.fiuntl.myshop.service.content.api.ContentConsumerService;
import com.funtl.myshop.service.user.api.UserConsumerService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("router")
public class RouterController {

    @Value("${services.ports.user}")
    private String userPort;

    @Value("${services.ports.content}")
    private String contentPort;


    @Reference(version = "${services.versions.user.v1}")
    private UserConsumerService userConsumerService;


    @Reference(version = "${services.versions.content.v1}")
    private ContentConsumerService contentConsumerService;


    @GetMapping("/user")
    public String redirect(String path) {
        //远程调用
        userConsumerService.info();
        // 本端是否为消费端，这里会返回true
        boolean isConsumerSide = RpcContext.getContext().isConsumerSide();
        if(isConsumerSide){
            // 获取最后一次调用的提供方IP地址
            String serverIP = RpcContext.getContext().getRemoteHost();
            System.out.println(serverIP);
            return "redirect:http://" + serverIP+":"+userPort+path;
        }

        // 获取当前服务配置信息，所有配置信息都将转换为URL的参数
//        String application = RpcContext.getContext().getUrl().getParameter("application");
        return null;

    }


    @GetMapping("/content")
    public String redirectContent(String path) {
        //远程调用
        userConsumerService.info();
        // 本端是否为消费端，这里会返回true
        boolean isConsumerSide = RpcContext.getContext().isConsumerSide();
        if(isConsumerSide){
            // 获取最后一次调用的提供方IP地址
            String serverIP = RpcContext.getContext().getRemoteHost();
            System.out.println(serverIP);
            return "redirect:http://" + serverIP+":"+contentPort+path;
        }

        // 获取当前服务配置信息，所有配置信息都将转换为URL的参数
//        String application = RpcContext.getContext().getUrl().getParameter("application");
        return null;

    }


}
